import React, { Component } from "react";
import Password from "./password.component";

class PasswordList extends Component {
  onDelete = (data) => {
    this.props.onDelete(data);
  };

  onEdit = (data) => {
    this.props.onEdit(data);
  };
  render() {
    const passwords = this.props.passwords;
    return (
      <div className="data">
        <table className="ui celled table">
          <thead>
            <tr>
              <th>Platform</th>
              <th>E-Mail</th>
              <th>Password</th>
              <th>Notes</th>
              <th>category</th>
              <th style={{ width: "148px" }}>Action</th>
            </tr>
          </thead>

          <tbody>
            {passwords.map((password, index) => {
              return (
                <Password
                  key={index}
                  password={password}
                  onDelete={this.onDelete}
                  onEdit={this.onEdit}
                />
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default PasswordList;
