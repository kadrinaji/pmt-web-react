import React, { Component } from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import pbkdf2 from "pbkdf2";

export default class Register extends Component {
  state = {};

  createVerifyHash = (email, password) => {
    var masterKey = pbkdf2.pbkdf2Sync(password, email, 10000, 128, "sha256");
    const mk = masterKey.toString("hex");
    return pbkdf2.pbkdf2Sync(mk, password, 10000, 64, "sha256");
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const data = {
      firstName: this.firstname.toString(),
      lastName: this.lastname.toString(),
      email: this.email.toString(),
      verifyHash: this.createVerifyHash(this.email, this.password).toString(
        "base64"
      ),
    };
    console.log(data);

    axios
      .post("/register", data)
      .then((res) => {
        console.log(res);
        this.setState({
          isRegistered: true,
          message: res,
          cls: "success",
        });
      })
      .catch((err) => {
        this.setState({
          message: err,
          cls: "danger",
        });
      });
  };

  render() {
    if (this.state.isRegistered) {
      return <Redirect to={"/login"} />;
    }

    let message = "";

    if (this.state.message) {
      const cls = "alert alert-" + this.state.cls;
      message = (
        <div className={cls} role="alert">
          {this.state.message}
        </div>
      );
    }
    return (
      <form onSubmit={this.handleSubmit}>
        {message}
        <h3>Sign Up</h3>

        <div className="form-group">
          <label>First Name</label>
          <input
            type="text"
            className="form-control"
            placeholder="First Name"
            onChange={(e) => (this.firstname = e.target.value)}
          />
        </div>

        <div className="form-group">
          <label>Last Name</label>
          <input
            type="text"
            className="form-control"
            placeholder="Last Name"
            onChange={(e) => (this.lastname = e.target.value)}
          />
        </div>

        <div className="form-group">
          <label>E-mail</label>
          <input
            type="email"
            className="form-control"
            placeholder="E-mail"
            onChange={(e) => (this.email = e.target.value)}
          />
        </div>

        <div className="form-group">
          <label>Password</label>
          <input
            type="password"
            className="form-control"
            placeholder="Password"
            onChange={(e) => (this.password = e.target.value)}
          />
        </div>

        <button className="btn btn-primary btn-block">Sign Up</button>
      </form>
    );
  }
}
