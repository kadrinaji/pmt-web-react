import React, { Component } from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import pbkdf2 from "pbkdf2";
import aes256 from "aes256";
export default class Login extends Component {
  // componentDidMount() {
  //     // POST request using axios with set headers
  //     const article = { title: 'React POST Request Example' };
  //     const headers = {
  //         'Authorization': 'Bearer my-token',
  //         'My-Custom-Header': 'foobar'
  //     };
  //     axios.post('https://reqres.in/api/articles', article, { headers })
  //         .then(response => this.setState({ articleId: response.data.id }));
  // }

  // login = () => {
  //     const data = {
  //         email: this.email,
  //         password: this.password
  //     };

  //     const res = axios.post('login', data, {
  //         'Authorization': 'Bearer' + res.data.token,
  //     }).then(response => {
  //         this.setState({
  //             loggedIn: true,
  //             message: response.data.message,
  //             cls: 'success'
  //         });
  //         this.props.setUser(response.data.user);
  //     }).catch(err => {
  //         this.setState({
  //             message: err.response.data.message,
  //             cls: 'danger'
  //         })
  //     })
  // }
  componentDidMount() {
    axios
      .get("user/session/key")
      .then((res) => {
        console.log(res.data);
        <Redirect to={"/"} />;
      })
      .catch((err) => console.log(err));
  }
  state = {};
  createVerifyHash = (email, password) => {
    return pbkdf2.pbkdf2Sync(
      this.createMasterKey(email, password),
      password,
      10000,
      64,
      "sha256"
    );
  };
  createMasterKey = (email, password) => {
    var masterKey = pbkdf2.pbkdf2Sync(password, email, 10000, 128, "sha256");
    const mk = masterKey.toString("hex");
    return mk;
  };
  handleSubmit = (e) => {
    e.preventDefault();

    const data = {
      email: this.email.toString(),
      verifyHash: this.createVerifyHash(this.email, this.password).toString(
        "base64"
      ),
    };

    axios
      .post("/login", data)
      .then((res) => {
        if (res.data.sessionKey) {
          let master = this.createMasterKey(this.email, this.password);
          let encryptedPass = aes256.encrypt(res.data.sessionKey, master);
          localStorage.setItem("key", encryptedPass);
          this.setState({
            loggedIn: true,
            message: res,
            cls: "success",
          });
          this.password = "GoodLuck Next Time";
          console.log(res);
        }
        axios.get("/user").then((resp) => {
          console.log(resp);
          this.props.setUser(resp.data);
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    if (this.state.loggedIn) {
      return <Redirect to={"/"} />;
    }

    let message = "";

    if (this.state.message) {
      const cls = "alert alert-" + this.state.cls;
      message = (
        <div className={cls} role="alert">
          {this.state.message}
        </div>
      );
    }
    return (
      <form onSubmit={this.handleSubmit}>
        <h3>Login</h3>
        {message}
        <div className="form-group">
          <label>E-mail</label>
          <input
            type="email"
            className="form-control"
            placeholder="E-mail"
            onChange={(e) => (this.email = e.target.value)}
          />
        </div>

        <div className="form-group">
          <label>Password</label>
          <input
            type="password"
            className="form-control"
            placeholder="Password"
            onChange={(e) => (this.password = e.target.value)}
          />
        </div>

        <button className="btn btn-primary btn-block">Login</button>
      </form>
    );
  }
}
