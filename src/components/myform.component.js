import React, { Component } from "react";

class MyForm extends Component {
  state = {
    form: {
      url: "",
      username: "",
      pass: "",
      notes: "",
      salt: "",
      category: "",
      oldUrl: "",
      oldUsername: "",
      isEdit: false,
    },
    btnName: "Save",
    btnClass: "ui primary button submit-button",
  };

  isEmpty(obj) {
    return Object.entries(obj).length === 0 && obj.constructor === Object;
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props && !this.isEmpty(this.props.password)) {
      const { url: oldUrl, username: oldUsername } = this.props.password;
      this.setState({
        form: {
          oldUrl,
          oldUsername,
          isEdit: true,
          ...this.props.password,
        },
        btnName: "Update",
        btnClass: "ui orange button submit-button",
      });
      //console.log('update');
    }
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    let form = this.state.form;
    form[name] = value;
    this.setState({ form });
  };

  onFormSubmit = (event) => {
    event.preventDefault();

    if (this.formValidation()) {
      this.props.onFormSubmit(this.state.form);
    }

    this.setState({
      btnName: "Save",
      btnClass: "ui primary button submit-button",
    });

    this.clearFormFields();
  };

  formValidation = () => {
    if (document.getElementsByName("url")[0].value === "") {
      alert("*Platform field is required*");
      return false;
    }

    if (document.getElementsByName("username")[0].value === "") {
      alert("*E-mail field is required*");
      return false;
    }

    if (document.getElementsByName("pass")[0].value === "") {
      alert("*Password field is required*");
      return false;
    }

    if (document.getElementsByName("notes")[0].value === "") {
      alert("*Description field is required*");
      return false;
    }
    if (document.getElementsByName("category")[0].value === "") {
      alert("*Category field is required*");
      return false;
    }
    return true;
  };

  clearFormFields = () => {
    // console.log("clear");
    // change form state
    this.setState({
      form: {
        url: "",
        username: "",
        pass: "",
        notes: "",
        salt: "",
        category: "",
        oldUrl: "",
        oldUsername: "",
        isEdit: false,
      },
    });

    // clear form fields
    document.querySelector(".form").reset();
  };

  render() {
    return (
      <form className="ui form">
        <div className="fields first">
          <div className="four wide field">
            <label>Platform</label>
            <input
              type="text"
              name="url"
              placeholder="url"
              onChange={this.handleChange}
              value={this.state.form.url}
            />
          </div>
          <div className="four wide field">
            <label>E-mail</label>
            <input
              type="text"
              name="username"
              placeholder="mert@gmail.com"
              onChange={this.handleChange}
              value={this.state.form.username}
            />
          </div>
          <div className="four wide field">
            <label>Password</label>
            <input
              type="password"
              name="pass"
              placeholder="Password"
              onChange={this.handleChange}
              value={this.state.form.pass}
            />
          </div>
          <div className="four wide field">
            <label>Notes</label>
            <input
              type="text"
              name="notes"
              placeholder="notes"
              onChange={this.handleChange}
              value={this.state.form.notes}
            />
          </div>
          <div className="four wide field">
            <label>Category</label>
            <input
              type="text"
              name="category"
              placeholder="category"
              onChange={this.handleChange}
              value={this.state.form.category}
            />
          </div>
          <div style={{ width: "0px", visibility: "hidden" }}>
            <input
              style={{ visibility: "hidden" }}
              name="old_url"
              value={this.state.form.url}
              onChange={() => {
                this.setState({ oldUrl: this.state.form.oldUrl });
              }}
            ></input>
            <input
              style={{ visibility: "hidden" }}
              name="old_username"
              value={this.state.form.username}
              onChange={() => {
                this.setState({ oldUsername: this.state.form.oldUsername });
              }}
            ></input>
          </div>
          <div className="four wide field">
            <button className={this.state.btnClass} onClick={this.onFormSubmit}>
              {this.state.btnName}
            </button>
          </div>
        </div>
      </form>
    );
  }
}

export default MyForm;
