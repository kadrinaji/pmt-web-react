import React, { Component } from "react";
import axios from "axios";
import MyForm from "./myform.component";
import PasswordList from "./passwordlist.component";
import Loader from "./loader.component";
import aes256 from "aes256";

export default class Home extends Component {
  _isMounted = false;
  state = {
    passwords: [],
    password: {},
    loader: false,
    Url: "/user/password",
  };
  decryptPasswords = (sessionkey, passwords) => {
    let masterKeyHash = localStorage.getItem("key");
    let masterkey = aes256.decrypt(sessionkey, masterKeyHash);
    return passwords.map((password) => {
      password.pass = aes256.decrypt(masterkey, password.pass);
      return password;
    });
  };

  getPasswords = async () => {
    this.setState({ loader: true });
    let response = await axios.get("/user/session/key");
    let sesskey = response.data.data.sessionKey;
    await axios
      .get(this.state.Url)
      .then((res) => {
        this._isMounted &&
          this.setState({
            passwords: this.decryptPasswords(sesskey, res.data.data),
            loader: false,
          });
      })
      .catch((err) => console.log(err));
  };

  deletePassword = async (data) => {
    this.setState({ loader: true });
    await axios
      .delete(`${this.state.Url}/?url=${data.url}&username=${data.username}`)
      .then((res) => console.log(res));
    this.getPasswords();
  };

  createPassword = async (data) => {
    this.setState({ loader: true });
    let masterKeyHash = localStorage.getItem("key");
    let res = await axios.get("/user/session/key");
    let sesskey = res.data.data.sessionKey;
    let masterkey = aes256.decrypt(sesskey, masterKeyHash);
    await axios.post(this.state.Url, {
      url: data.url,
      username: data.username,
      pass: aes256.encrypt(masterkey, data.pass),
      notes: data.notes,
      salt: data.salt,
      category: data.category,
    });
    this.getPasswords();
  };

  editPassword = async (data) => {
    this.setState({ password: {}, loader: true });
    const { oldUrl, oldUsername } = { ...data };
    delete data.isEdit;
    delete data.oldUrl;
    delete data.oldUsername;
    let masterKeyHash = localStorage.getItem("key");
    let res = await axios.get("/user/session/key");
    let sesskey = res.data.data.sessionKey;
    let masterkey = aes256.decrypt(sesskey, masterKeyHash);
    data.pass = aes256.encrypt(masterkey, data.pass);

    await axios
      .put(`${this.state.Url}`, {
        url: oldUrl,
        username: oldUsername,
        update: { ...data },
      })
      .then((res) => console.log(res));
    this.getPasswords();
  };

  componentDidMount() {
    this._isMounted = true;
    this._isMounted && this.getPasswords();
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  onDelete = (data) => {
    this.deletePassword(data);
  };

  onEdit = (data) => {
    this.setState({ password: data });
  };

  onFormSubmit = (data) => {
    if (data.isEdit) {
      this.editPassword(data);
    } else {
      this.createPassword(data);
    }
  };

  render() {
    if (this.props.user) {
      return (
        <div>
          <div className="ui main container">
            <MyForm
              onFormSubmit={this.onFormSubmit}
              password={this.state.password}
            />
            {this.state.loader ? <Loader /> : ""}
            <PasswordList
              passwords={this.state.passwords}
              onDelete={this.onDelete}
              onEdit={this.onEdit}
            />
          </div>
        </div>
      );
    } else {
      return <h2 className="not-loggedin">You are not logged in</h2>;
    }
  }
}
