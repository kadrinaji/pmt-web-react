import React, { Component } from "react";

class Password extends Component {
  onEdit = () => {
    this.props.onEdit(this.props.password);
  };

  onDelete = () => {
    this.props.onDelete(this.props.password);
  };

  render() {
    const { url, username, pass, notes, category } = this.props.password;

    return (
      <tr>
        <td>{url}</td>
        <td>{username}</td>
        <td>{pass}</td>
        <td>{notes}</td>
        <td>{category}</td>
        <td>
          <button className="mini ui button" onClick={this.onEdit}>
            Edit
          </button>
          <button className="mini ui button" onClick={this.onDelete}>
            Delete
          </button>
        </td>
      </tr>
    );
  }
}

export default Password;
